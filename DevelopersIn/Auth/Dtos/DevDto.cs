﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Auth.Models;
namespace Auth.Dtos
{
    public class DevDto
    {
        public byte Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        public string Expertise { get; set; }
        public string Experience { get; set; }
        public string Qualification { get; set; }
        [Required(ErrorMessage = "Please enter a date for when the developer is available from")]
        public DateTime AvailableFrom { get; set; }
        [Required(ErrorMessage = "Please enter a date for when the developer is available till")]
        public DateTime AvailableTill { get; set; }
       // [RegularExpression(@"^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$",
       //ErrorMessage = "Enter a valid email.")]
        [Required]
        public string Email { get; set; }
        public string ProjectName { get; set; }
        public IEnumerable<int> TagIds { get; set; }
    }
}