﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace Auth.Dtos
{
    public class ProjectDto
    {
        public byte Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }
        public int? Budget { get; set; }
        public string Duration { get; set; }
        [Required(ErrorMessage = "Deadline Date is required")]
        public DateTime Deadline { get; set; }
       
        public DateTime? StartDate { get; set; }
      
        public DateTime? EndDate { get; set; }
        public IEnumerable<int> TagIds { get; set; }
        public IEnumerable<string> TagNames { get; set; }
    }
}