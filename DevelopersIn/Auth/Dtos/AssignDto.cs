﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Auth.Dtos
{
    public class AssignDto
    {
        public int DevId { get; set; }
        public int ProjId { get; set; }
    }
}