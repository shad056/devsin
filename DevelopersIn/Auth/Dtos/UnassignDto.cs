﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Auth.Models;
namespace Auth.Dtos
{
    public class UnassignDto
    {
        public byte Id { get; set; }
        public string Name { get; set; }
        public string Expertise { get; set; }
        public string Experience { get; set; }
        public string Qualification { get; set; }
        public DateTime AvailableFrom { get; set; }
        public DateTime AvailableTill { get; set; }
        // [RegularExpression(@"^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$",
        //ErrorMessage = "Enter a valid email.")]
        public string Email { get; set; }
        public Projects Projects { get; set; }
        public byte ProjectsId { get; set; }
        public IEnumerable<int> TagIds { get; set; }
    }
}