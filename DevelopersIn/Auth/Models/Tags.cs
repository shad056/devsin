﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace Auth.Models
{
    public class Tags
    {
      
        public byte Id { get; set; }
        [Required(ErrorMessage ="TagName is required")]
        public string Name { get; set; }
        public Projects Project { get; set; }
    }
}