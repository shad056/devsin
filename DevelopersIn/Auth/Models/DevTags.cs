﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Auth.Models
{
    public class DevTags
    {
        public int Id { get; set; }
        public int DevId { get; set; }
        public int TagId { get; set; }
    }
}