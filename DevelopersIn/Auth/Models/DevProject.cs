﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Auth.Models
{
    public class DevProject
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int DevId { get; set; }
    }
}