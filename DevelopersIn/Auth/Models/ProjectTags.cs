﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Auth.Models;
namespace Auth.Models
{
    public class ProjectTags
    {
        public byte Id { get; set; }
        public int TagId { get; set; }
        public byte ProjectId { get; set; }
    }
}