﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Auth.Models
{
    public class Triggers
    {
        public int Id { get; set; }
        public int TriggerState { get; set; }
    }
}