﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Auth.Models;
namespace Auth.ViewModel
{
    public class ProjectDetailViewModel
    {
        public Projects Projects { get; set; }
        public List<Tags> Tag { get; set; }
    }
}