﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Auth.Models;
namespace Auth.ViewModel
{
    public class ProjectAddViewModel
    {
        public Projects Project { get; set; }
        public List<Tags> TagIds { get; set; }
    }
}