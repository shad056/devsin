﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Auth.Models;
namespace Auth.ViewModel
{
    public class DevDetailViewModel
    {
        public Developers Developer { get; set; }
        public IEnumerable<Tags> Tag { get; set; }
    }
}