﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Auth.Models;
using Auth.ViewModel;
using System.Web.Http;
using System.Data.Entity.Validation;
namespace Auth.Controllers
{
    public class RecomController : Controller
    { public ApplicationDbContext _context;
        public RecomController()
        {
            _context = new ApplicationDbContext();
        }
        // GET: Recom
        //[Route("Recom/Index/{id}")]
        public ActionResult Index(int id)
        {
            var trigger = _context.Trigger.Single(c => c.Id == 1);
            trigger.TriggerState = 0;
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                Console.WriteLine(e);
            }
            var proj = _context.Project.Single(c => c.Id == id);
            var tag = _context.ProjectTags.Where(m => m.ProjectId == id).ToList();
            List<Developers> dd = new List<Developers>();
            foreach (var ss in tag)
            {
                var sd = _context.DevTags.Where(c => c.TagId == ss.TagId).ToList();
                foreach (var sdf in sd)
                {
                    var tt = _context.Developer.Where(c => c.Id == sdf.DevId).ToList();

                    foreach (var ff in tt)
                    {
                        if (ff.AvailableFrom.Date < proj.Deadline.Date && ff.AvailableTill.Date >= proj.Deadline.Date)
                            dd.Add(ff);
                    }
                }
            }
            var hh = new RecomDevViewModel {Developers = dd };
           
            return View(hh);
        }
    }
}