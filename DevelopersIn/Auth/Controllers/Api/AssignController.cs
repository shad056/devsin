﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Auth.Models;
using Auth.Dtos;
using Auth.ViewModel;
using System.Data.Entity;
using System.Data.Entity.Validation;
using AutoMapper;
namespace Auth.Controllers.Api
{
    public class AssignController : ApiController
    {
        public ApplicationDbContext _context;
        public AssignController()
        {
            _context = new ApplicationDbContext();
        }



        [Route("Api/Assign/AssignProj")]
        [HttpPost]
        public IHttpActionResult AssignProj(AssignDto ast)
        {
            var dev = _context.Developer.Single(c => c.Id == ast.DevId);
            var projId = _context.DevProject.OrderByDescending(c => c.Id).First();
            var proj = new DevProject {DevId = dev.Id, ProjectId = ast.ProjId  };
            proj.Id = projId.Id;
            proj.Id++;
            var pros = _context.DevProject.SingleOrDefault(c => c.DevId == dev.Id);
            if (pros != null)
                return BadRequest("Developer is already assigned this project");
            else
            {
                _context.DevProject.Add(proj);
                var projname = _context.Project.Single(c => c.Id == ast.ProjId);
                dev.ProjectName = projname.Name;
                //dev.Projects.Ad = (byte)ast.ProjId;

                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    Console.WriteLine(e);
                }
            }
            return Created(new Uri(Request.RequestUri + "/" + ast.DevId), ast);
        }

        [Route("Api/Assign/Unassign")]
        [HttpPost]
        public IHttpActionResult Unassign(Developers unas)
        {
            var dev = _context.Developer.Single(c => c.Id == unas.Id);
            //unas.Name = dev.Name;
            //unas.Qualification = dev.Qualification;
            //unas.Expertise = dev.Expertise;
            //unas.Experience = dev.Experience;
            //unas.AvailableFrom = dev.AvailableFrom;
            //unas.AvailableTill = dev.AvailableTill;
            //unas.Email = dev.Email;
            //_context.Developer.Remove(dev);
            ////var de = Mapper.Map<UnassignDto, Developers>(unas);
            //_context.Developer.Add(unas);
            dev.ProjectName = null;
            var proj = _context.DevProject.Single(c => c.DevId == dev.Id);
            _context.DevProject.Remove(proj);
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                Console.WriteLine(e);
            }
            //var ss = _context.Developer.Include(d => d.Projects).Single(c => c.Id == unas.Id);
            //ss.Projects.Id = 0;
            return Ok();
        }
    }
}
