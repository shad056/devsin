﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Routing;
using Auth.Models;
using System.Web;
using Auth.ViewModel;
using System.Data.Entity.Validation;
using Auth.Dtos;
using AutoMapper;

namespace Auth.Controllers.Api
{
    public class ProjectController : ApiController
    {
        public ApplicationDbContext _context;
        public ProjectController()
        {
            _context = new ApplicationDbContext();
        }
        public IHttpActionResult GetTags(string query=null)
        {
            var tag = _context.Tag.ToList();
            if (!String.IsNullOrWhiteSpace(query))
                tag = tag.Where(c => c.Name.Contains(query)).ToList();
            return Ok(tag);
        }
        [Route("Api/Project/GetProjects")]
        public IHttpActionResult GetProjects(string query = null)
        {
            var project = _context.Project.ToList();
            if (!String.IsNullOrWhiteSpace(query))
                project = project.Where(m => m.Name.Contains(query)).ToList();
            return Ok(project);
        }

        [HttpPost]
        public IHttpActionResult SaveProject(ProjectDto projectdto)
        { var ph = _context.Project.OrderByDescending(x => x.Id).FirstOrDefault();
            projectdto.Id = ph.Id;
            projectdto.Id++;

            var proj = Mapper.Map<ProjectDto, Projects>(projectdto);
            _context.Project.Add(proj);
           

            foreach (var ss in projectdto.TagIds)
            {
                var st = new ProjectTags
                {
                    ProjectId = projectdto.Id,
                    TagId = ss
                };
                var ts = _context.ProjectTags.OrderByDescending(c => c.Id).FirstOrDefault();
                st.Id = ts.Id;
                st.Id++;
                _context.ProjectTags.Add(st);
                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    Console.WriteLine(e);
                }
            }
            var hh = _context.Trigger.Single(c => c.Id == 1);
            hh.TriggerState = 1;
            
            //var st = new ProjectTags {Project = proj};
            //var hh = _context.ProjectTags.OrderByDescending(c => c.Id).FirstOrDefault();
            //st.Id = hh.Id;
            //st.Id++;
            //_context.ProjectTags.Add(st);
           
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                Console.WriteLine(e);
            }

             return Created(new Uri(Request.RequestUri + "/" + projectdto.Id), projectdto);
            //return RedirectToRoute("Recomroute", new {id = projectdto.Id });


        }

        [Route("api/Project/EditProject")]
        [HttpPost]

        public IHttpActionResult EditProject(ProjectDto pj)
        {
            var project = _context.Project.Single(c => c.Id == pj.Id);
            project.Name = pj.Name;
            project.Description = pj.Description;
            project.Budget = pj.Budget;
            project.Duration = pj.Duration;
            project.Deadline = pj.Deadline;
            project.StartDate = pj.StartDate;
            project.EndDate = pj.EndDate;
            if (pj.TagIds != null)
            {
                foreach (var ss in pj.TagIds)
                {
                    var st = new ProjectTags
                    {
                        ProjectId = pj.Id,
                        TagId = ss
                    };
                    var ts = _context.ProjectTags.OrderByDescending(c => c.Id).FirstOrDefault();
                    st.Id = ts.Id;
                    st.Id++;
                    _context.ProjectTags.Add(st);
                    try
                    {
                        _context.SaveChanges();
                    }
                    catch (DbEntityValidationException e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                Console.WriteLine(e);
            }
            return Created(new Uri(Request.RequestUri + "/" + pj.Id), pj);
        }

    }
}
