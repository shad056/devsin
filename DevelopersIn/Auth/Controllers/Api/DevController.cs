﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Auth.Models;
using Auth.ViewModel;
using System.Data.Entity.Validation;
using Auth.Dtos;
using AutoMapper;
using System.Text;
using System.Data.Entity;

namespace Auth.Controllers.Api
{
    public class DevController : ApiController
    {
        private ApplicationDbContext _context;

        public DevController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        [Route("Api/Dev/GetDev")]
        public IHttpActionResult GetDev()
        {
            var dev = _context.Developer.ToList();
            return Ok(dev);
        }
        [HttpPost]
        [Route("Api/Dev/SaveDev")]
        public IHttpActionResult SaveDev(DevDto dev)
        {
            var devs = _context.Developer.OrderByDescending(x => x.Id).FirstOrDefault();
          
                dev.Id = devs.Id;
                dev.Id++;
                var de = Mapper.Map<DevDto, Developers>(dev);
                _context.Developer.Add(de);
                foreach(var ss in dev.TagIds)
                {
                    var st = new DevTags
                    {
                        DevId = dev.Id,
                        TagId = ss
                    };
                    var ts = _context.DevTags.OrderByDescending(c => c.Id).FirstOrDefault();
                    st.Id = ts.Id;
                    st.Id++;
                    _context.DevTags.Add(st);
                    try
                    {
                        _context.SaveChanges();
                    }
                    catch (DbEntityValidationException e)
                    {
                        Console.WriteLine(e);
                    }
                }
                
                // _context.Developer.Add(dev);

                try
                {
                    _context.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    Console.WriteLine(e);
                }
                return Created(new Uri(Request.RequestUri + "/" + dev.Id), dev);
  
        }
        [HttpPost]
        [Route("Api/Dev/EditDev")]
        public IHttpActionResult EditDev(DevDto dev)
        {

            var devs = _context.Developer.Single(c => c.Id == dev.Id);
            devs.Id = dev.Id;
            devs.Name = dev.Name;
            devs.Expertise = dev.Expertise;
            devs.Experience = dev.Experience;
            //devs.Skills = dev.Skills;
            devs.Qualification = dev.Qualification;
            devs.AvailableFrom = dev.AvailableFrom;
            devs.AvailableTill = dev.AvailableTill;
            devs.Email = dev.Email;
            if (dev.TagIds != null)
            {
                foreach (var ss in dev.TagIds)
                {
                    var st = new DevTags
                    {
                        DevId = dev.Id,
                        TagId = ss
                    };
                    var ts = _context.DevTags.OrderByDescending(c => c.Id).FirstOrDefault();
                    st.Id = ts.Id;
                    st.Id++;
                    _context.DevTags.Add(st);
                    try
                    {
                        _context.SaveChanges();
                    }
                    catch (DbEntityValidationException e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                Console.WriteLine(e);
            }
            return Created(new Uri(Request.RequestUri + "/" + dev.Id), dev);
        }
    }
}
