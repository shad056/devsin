﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Auth.Models;
using Auth.ViewModel;
using Auth.Dtos;
using System.Data.Entity.Validation;
namespace Auth.Controllers
{
    public class ProjectController : Controller
    {
        public ApplicationDbContext _context;
        public ProjectController()
        {
            _context = new ApplicationDbContext();
        }
        // GET: Project
        public ActionResult Index()
        {
            var proj = _context.Project.ToList();
            var trigger = _context.Trigger.Single(c => c.Id == 1);
            if(trigger.TriggerState == 1)
            {
                var lastaddedproject = _context.Project.OrderByDescending(c => c.Id).First();
                return RedirectToAction("Index", "Recom", new { id = lastaddedproject.Id });
            }
            return View(proj);
         
          
        }

        public ActionResult Detail(byte id)
        {
            
            var proj = _context.Project.Single(c => c.Id == id);
            var tag = _context.ProjectTags.Where(m => m.ProjectId == id);
            List<Tags> dd = new List<Tags>();
            foreach (var ss in tag)
            {
                var sd = _context.Tag.Where(m => m.Id == ss.TagId).ToList();
                foreach(var hh in sd)
                {
                    dd.Add(hh);
                }
            }
            //var tag = _context.Tag.Where(m => m.Projects.Any(c => c.Id == id));
            var vm = new ProjectDetailViewModel { Projects = proj, Tag = dd
            };
        
            //proj.Tags = tag.ToList();
            return View(vm);
        }

        public ActionResult Add()
        {
            return View();
        }
        public ActionResult Edit(int id)
        {
            var project = _context.Project.Single(c => c.Id == id);
            //var ids = project.Id;
            var jj = _context.ProjectTags.Where(m => m.ProjectId == project.Id).ToList();
           
            List<Tags> sum = new List<Tags>();
            foreach (var st in jj)
            {
                var mm = _context.Tag.Single(c => c.Id == st.TagId);
                sum.Add(mm);
            }
            var sj = new ProjectDetailViewModel { Projects = project, Tag = sum.ToList() };
           
            return View(sj);
        }

        public ActionResult Delete(int id)
        {
            var pj = _context.Project.Single(c => c.Id == id);
            var tags = _context.ProjectTags.Where(m => m.ProjectId == pj.Id).ToList();
            _context.Project.Remove(pj);
            foreach (var ss in tags)
            {
                _context.ProjectTags.Remove(ss);
            }
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                Console.WriteLine(e);
            }
            return RedirectToAction("Index", "Project");
        }
    }
}