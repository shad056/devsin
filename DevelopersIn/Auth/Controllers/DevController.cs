﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Auth.Models;
using System.Data.Entity.Validation;
using Auth.ViewModel;

namespace Auth.Controllers
{
    public class DevController : Controller
    {
        private ApplicationDbContext _context;

        public DevController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Dev
        public ActionResult Index()
        {
            var dev = _context.Developer.ToList();
            return View(dev);
        }
        public ActionResult Detail(int id)
        {
            var dev = _context.Developer.Single(c => c.Id == id);
            var tag = _context.DevTags.Where(m => m.DevId == dev.Id).ToList();
            List<Tags> dd = new List<Tags>();
            foreach(var ss in tag)
            {
                var sd = _context.Tag.Where(m => m.Id == ss.TagId).ToList();
                foreach(var ff in sd)
                {
                    dd.Add(ff);
                }
            }
            var gg = new DevDetailViewModel { Developer = dev, Tag = dd };
            return View(gg);
        } 

        public ActionResult Add()
        {
            return View();
        }
        public ActionResult Edit(int id)
        {
            var dev = _context.Developer.Single(c => c.Id == id);
            List<Tags> dd = new List<Tags>();
            var tag = _context.DevTags.Where(m => m.DevId == dev.Id).ToList();
           
            foreach (var ss in tag)
            {
                var sd = _context.Tag.Where(m => m.Id == ss.TagId).ToList();
                foreach(var tt in sd)
                {
                    dd.Add(tt);
                }
            }
            var hh = new DevDetailViewModel { Developer = dev, Tag = dd };
            return View(hh);
        }
        public ActionResult Delete(int id)
        {
            var dev = _context.Developer.Single(c => c.Id == id);
            _context.Developer.Remove(dev);
            var tag = _context.DevTags.Where(m => m.DevId == dev.Id).ToList();
            foreach(var ss in tag)
            {
                _context.DevTags.Remove(ss);
            }
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                Console.WriteLine(e);
            }
            return RedirectToAction("Index", "Dev");
            
        }
        [ValidateAntiForgeryToken]
        public ActionResult Save(Developers dev)
        {
            if (!ModelState.IsValid)
                return View("Add");

            var devs = _context.Developer.OrderByDescending(x => x.Id).FirstOrDefault();
            dev.Id = devs.Id;
            dev.Id++;
            _context.Developer.Add(dev);

            try
            {
                _context.SaveChanges();
            }
            catch(DbEntityValidationException e)
            {
                Console.WriteLine(e);
            }
            return RedirectToAction("Index","Dev");
        }

        public ActionResult SaveEdit(Developers dev)
        {

            var devs = _context.Developer.Single(c => c.Id == dev.Id);

            devs.Name = dev.Name;
            devs.Expertise = dev.Expertise;
            devs.Experience = dev.Experience;
            //devs.Skills = dev.Skills;
            devs.Qualification = dev.Qualification;
            devs.AvailableFrom = dev.AvailableFrom;
            devs.AvailableTill = dev.AvailableTill;
            devs.Email = dev.Email;
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                Console.WriteLine(e);
            }
            return RedirectToAction("Index", "Dev");
        }
    }
}