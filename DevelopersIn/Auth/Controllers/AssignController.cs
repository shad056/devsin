﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Auth.Models;
using Auth.ViewModel;
using System.Data.Entity;
namespace Auth.Controllers
{
    public class AssignController : Controller
    {
        public ApplicationDbContext _context;
        public AssignController()
        {
            _context = new ApplicationDbContext();
        }
        // GET: Assign
        public ActionResult Index()
        {
            //var devs = _context.Developer.ToList();
            return View();
        }
        
        public ActionResult Edit(int id)
        {
            int sample = 0;
            var dev = _context.Developer.Single(c => c.Id == id);
            var proj = _context.DevProject.SingleOrDefault(c => c.DevId == dev.Id);
            if(proj == null)
            {
                sample = 0;
            }
            else
            {
                sample = 1;
            }
            var vm = new AssignEditViewModel { Developer = dev, Sample = sample };
            return View(vm);
        }
    }
}