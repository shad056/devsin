﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Auth.Models;
using Auth.Dtos;
namespace Auth.App_Start
{
    public class MappingProfile:Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<Projects, ProjectDto>().ForMember(c => c.TagIds, d => d.Ignore()).ForMember(e => e.TagNames, f => f.Ignore());
            Mapper.CreateMap<ProjectDto, Projects>().ForSourceMember(c=>c.TagIds, d => d.Ignore()).ForSourceMember(e => e.TagNames, f => f.Ignore());
            Mapper.CreateMap<Developers, DevDto>().ForMember(c=> c.ProjectName, d=>d.Ignore()).ForMember(e => e.TagIds, f => f.Ignore());
            Mapper.CreateMap<DevDto, Developers>().ForSourceMember(c=> c.ProjectName, d=>d.Ignore()).ForSourceMember(e => e.TagIds, f => f.Ignore());
            Mapper.CreateMap<Developers, UnassignDto>().ForMember(c => c.Projects, d => d.Ignore()).ForMember(e => e.TagIds, f => f.Ignore());
            Mapper.CreateMap<UnassignDto, Developers>().ForSourceMember(c => c.Projects, d => d.Ignore()).ForSourceMember(e => e.TagIds, f => f.Ignore());
        }
    }
}