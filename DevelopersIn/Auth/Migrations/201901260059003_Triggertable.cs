namespace Auth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Triggertable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Triggers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TriggerState = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Triggers");
        }
    }
}
