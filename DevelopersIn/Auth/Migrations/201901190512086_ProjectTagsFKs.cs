namespace Auth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProjectTagsFKs : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tags", "Project_Id", c => c.Byte());
            CreateIndex("dbo.Tags", "Project_Id");
            AddForeignKey("dbo.Tags", "Project_Id", "dbo.Projects", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tags", "Project_Id", "dbo.Projects");
            DropIndex("dbo.Tags", new[] { "Project_Id" });
            DropColumn("dbo.Tags", "Project_Id");
        }
    }
}
