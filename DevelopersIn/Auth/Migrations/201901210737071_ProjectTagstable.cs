namespace Auth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProjectTagstable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectTags",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        TagId = c.Byte(nullable: false),
                        ProjectId = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ProjectTags");
        }
    }
}
