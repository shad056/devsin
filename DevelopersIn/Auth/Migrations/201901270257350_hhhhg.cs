namespace Auth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class hhhhg : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Developers", "Projects_Id", "dbo.Projects");
            DropIndex("dbo.Developers", new[] { "Projects_Id" });
            CreateTable(
                "dbo.DevProjects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProjectId = c.Int(nullable: false),
                        DevId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Developers", "ProjectName", c => c.String());
            DropColumn("dbo.Developers", "Projects_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Developers", "Projects_Id", c => c.Byte());
            DropColumn("dbo.Developers", "ProjectName");
            DropTable("dbo.DevProjects");
            CreateIndex("dbo.Developers", "Projects_Id");
            AddForeignKey("dbo.Developers", "Projects_Id", "dbo.Projects", "Id");
        }
    }
}
