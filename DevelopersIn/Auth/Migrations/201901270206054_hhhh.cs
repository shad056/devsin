namespace Auth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class hhhh : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Developers", "ProjectsId", "dbo.Projects");
            DropIndex("dbo.Developers", new[] { "ProjectsId" });
            RenameColumn(table: "dbo.Developers", name: "ProjectsId", newName: "Projects_Id");
            AlterColumn("dbo.Developers", "Projects_Id", c => c.Byte());
            CreateIndex("dbo.Developers", "Projects_Id");
            AddForeignKey("dbo.Developers", "Projects_Id", "dbo.Projects", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Developers", "Projects_Id", "dbo.Projects");
            DropIndex("dbo.Developers", new[] { "Projects_Id" });
            AlterColumn("dbo.Developers", "Projects_Id", c => c.Byte(nullable: false));
            RenameColumn(table: "dbo.Developers", name: "Projects_Id", newName: "ProjectsId");
            CreateIndex("dbo.Developers", "ProjectsId");
            AddForeignKey("dbo.Developers", "ProjectsId", "dbo.Projects", "Id", cascadeDelete: true);
        }
    }
}
