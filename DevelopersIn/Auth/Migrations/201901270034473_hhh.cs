namespace Auth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class hhh : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Developers", "Projects_Id", "dbo.Projects");
            DropIndex("dbo.Developers", new[] { "Projects_Id" });
            RenameColumn(table: "dbo.Developers", name: "Projects_Id", newName: "ProjectsId");
            AlterColumn("dbo.Developers", "ProjectsId", c => c.Byte(nullable: false));
            CreateIndex("dbo.Developers", "ProjectsId");
            AddForeignKey("dbo.Developers", "ProjectsId", "dbo.Projects", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Developers", "ProjectsId", "dbo.Projects");
            DropIndex("dbo.Developers", new[] { "ProjectsId" });
            AlterColumn("dbo.Developers", "ProjectsId", c => c.Byte());
            RenameColumn(table: "dbo.Developers", name: "ProjectsId", newName: "Projects_Id");
            CreateIndex("dbo.Developers", "Projects_Id");
            AddForeignKey("dbo.Developers", "Projects_Id", "dbo.Projects", "Id");
        }
    }
}
