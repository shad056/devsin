namespace Auth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class hh : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Developers", "ProjectsId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Developers", "ProjectsId", c => c.Int(nullable: false));
        }
    }
}
