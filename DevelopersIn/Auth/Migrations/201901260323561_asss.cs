namespace Auth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class asss : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Developers", "Tags_Id", "dbo.Tags");
            DropIndex("dbo.Developers", new[] { "Tags_Id" });
            CreateTable(
                "dbo.DevTags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DevId = c.Int(nullable: false),
                        TagId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.Developers", "Tags_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Developers", "Tags_Id", c => c.Byte());
            DropTable("dbo.DevTags");
            CreateIndex("dbo.Developers", "Tags_Id");
            AddForeignKey("dbo.Developers", "Tags_Id", "dbo.Tags", "Id");
        }
    }
}
