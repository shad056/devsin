namespace Auth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _as : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Developers", "Skills");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Developers", "Skills", c => c.String());
        }
    }
}
