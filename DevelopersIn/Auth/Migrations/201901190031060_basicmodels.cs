namespace Auth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class basicmodels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Developers",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Name = c.String(nullable: false),
                        Expertise = c.String(),
                        Experience = c.String(),
                        Qualification = c.String(),
                        Skills = c.String(),
                        AvailableFrom = c.DateTime(),
                        AvailableTill = c.DateTime(),
                        Email = c.String(nullable: false),
                        Projects_Id = c.Byte(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Projects", t => t.Projects_Id)
                .Index(t => t.Projects_Id);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Name = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        Budget = c.Int(),
                        Duration = c.String(),
                        Deadline = c.DateTime(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TagsDevelopers",
                c => new
                    {
                        Tags_Id = c.Byte(nullable: false),
                        Developers_Id = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tags_Id, t.Developers_Id })
                .ForeignKey("dbo.Tags", t => t.Tags_Id, cascadeDelete: true)
                .ForeignKey("dbo.Developers", t => t.Developers_Id, cascadeDelete: true)
                .Index(t => t.Tags_Id)
                .Index(t => t.Developers_Id);
            
         
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.TagsDevelopers", "Developers_Id", "dbo.Developers");
            DropForeignKey("dbo.TagsDevelopers", "Tags_Id", "dbo.Tags");
            DropForeignKey("dbo.Developers", "Projects_Id", "dbo.Projects");
            DropIndex("dbo.TagsDevelopers", new[] { "Developers_Id" });
            DropIndex("dbo.TagsDevelopers", new[] { "Tags_Id" });
            DropIndex("dbo.Developers", new[] { "Projects_Id" });
            DropTable("dbo.TagsDevelopers");
            DropTable("dbo.Tags");
            DropTable("dbo.Projects");
            DropTable("dbo.Developers");
        }
    }
}
