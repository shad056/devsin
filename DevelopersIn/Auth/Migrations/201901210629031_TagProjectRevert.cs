namespace Auth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TagProjectRevert : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TagsProjects", "Tags_Id", "dbo.Tags");
            DropForeignKey("dbo.TagsProjects", "Projects_Id", "dbo.Projects");
            DropIndex("dbo.TagsProjects", new[] { "Tags_Id" });
            DropIndex("dbo.TagsProjects", new[] { "Projects_Id" });
            AddColumn("dbo.Tags", "Project_Id", c => c.Byte());
            CreateIndex("dbo.Tags", "Project_Id");
            AddForeignKey("dbo.Tags", "Project_Id", "dbo.Projects", "Id");
            DropTable("dbo.TagsProjects");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TagsProjects",
                c => new
                    {
                        Tags_Id = c.Byte(nullable: false),
                        Projects_Id = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tags_Id, t.Projects_Id });
            
            DropForeignKey("dbo.Tags", "Project_Id", "dbo.Projects");
            DropIndex("dbo.Tags", new[] { "Project_Id" });
            DropColumn("dbo.Tags", "Project_Id");
            CreateIndex("dbo.TagsProjects", "Projects_Id");
            CreateIndex("dbo.TagsProjects", "Tags_Id");
            AddForeignKey("dbo.TagsProjects", "Projects_Id", "dbo.Projects", "Id", cascadeDelete: true);
            AddForeignKey("dbo.TagsProjects", "Tags_Id", "dbo.Tags", "Id", cascadeDelete: true);
        }
    }
}
