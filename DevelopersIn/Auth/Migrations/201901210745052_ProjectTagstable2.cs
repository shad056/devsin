namespace Auth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProjectTagstable2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ProjectTags", "TagId", c => c.Int(nullable: false));
            AlterColumn("dbo.ProjectTags", "ProjectId", c => c.Byte(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProjectTags", "ProjectId", c => c.Int(nullable: false));
            AlterColumn("dbo.ProjectTags", "TagId", c => c.Byte(nullable: false));
        }
    }
}
