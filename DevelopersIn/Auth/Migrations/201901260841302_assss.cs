namespace Auth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class assss : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Developers", "ProjectsId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Developers", "ProjectsId");
        }
    }
}
