namespace Auth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProjectTagsRevert : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProjectTags", "Project_Id", "dbo.Projects");
            DropIndex("dbo.ProjectTags", new[] { "Project_Id" });
            AddColumn("dbo.ProjectTags", "TagId", c => c.Int(nullable: false));
            AddColumn("dbo.ProjectTags", "ProjectId", c => c.Byte(nullable: false));
            DropColumn("dbo.ProjectTags", "Project_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProjectTags", "Project_Id", c => c.Byte());
            DropColumn("dbo.ProjectTags", "ProjectId");
            DropColumn("dbo.ProjectTags", "TagId");
            CreateIndex("dbo.ProjectTags", "Project_Id");
            AddForeignKey("dbo.ProjectTags", "Project_Id", "dbo.Projects", "Id");
        }
    }
}
