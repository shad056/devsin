namespace Auth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProjectTagsFK : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectTags", "Project_Id", c => c.Byte());
            CreateIndex("dbo.ProjectTags", "Project_Id");
            AddForeignKey("dbo.ProjectTags", "Project_Id", "dbo.Projects", "Id");
            DropColumn("dbo.ProjectTags", "TagId");
            DropColumn("dbo.ProjectTags", "ProjectId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProjectTags", "ProjectId", c => c.Byte(nullable: false));
            AddColumn("dbo.ProjectTags", "TagId", c => c.Int(nullable: false));
            DropForeignKey("dbo.ProjectTags", "Project_Id", "dbo.Projects");
            DropIndex("dbo.ProjectTags", new[] { "Project_Id" });
            DropColumn("dbo.ProjectTags", "Project_Id");
        }
    }
}
