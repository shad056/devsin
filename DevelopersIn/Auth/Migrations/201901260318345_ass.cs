namespace Auth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ass : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TagsDevelopers", "Tags_Id", "dbo.Tags");
            DropForeignKey("dbo.TagsDevelopers", "Developers_Id", "dbo.Developers");
            DropIndex("dbo.TagsDevelopers", new[] { "Tags_Id" });
            DropIndex("dbo.TagsDevelopers", new[] { "Developers_Id" });
            AddColumn("dbo.Developers", "Tags_Id", c => c.Byte());
            CreateIndex("dbo.Developers", "Tags_Id");
            AddForeignKey("dbo.Developers", "Tags_Id", "dbo.Tags", "Id");
            DropTable("dbo.TagsDevelopers");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TagsDevelopers",
                c => new
                    {
                        Tags_Id = c.Byte(nullable: false),
                        Developers_Id = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tags_Id, t.Developers_Id });
            
            DropForeignKey("dbo.Developers", "Tags_Id", "dbo.Tags");
            DropIndex("dbo.Developers", new[] { "Tags_Id" });
            DropColumn("dbo.Developers", "Tags_Id");
            CreateIndex("dbo.TagsDevelopers", "Developers_Id");
            CreateIndex("dbo.TagsDevelopers", "Tags_Id");
            AddForeignKey("dbo.TagsDevelopers", "Developers_Id", "dbo.Developers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.TagsDevelopers", "Tags_Id", "dbo.Tags", "Id", cascadeDelete: true);
        }
    }
}
