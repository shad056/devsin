namespace Auth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cc : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Developers", "AvailableFrom", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Developers", "AvailableTill", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Developers", "AvailableTill", c => c.DateTime());
            AlterColumn("dbo.Developers", "AvailableFrom", c => c.DateTime());
        }
    }
}
