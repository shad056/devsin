namespace Auth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TagProjecttable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Tags", "Project_Id", "dbo.Projects");
            DropIndex("dbo.Tags", new[] { "Project_Id" });
            CreateTable(
                "dbo.TagsProjects",
                c => new
                    {
                        Tags_Id = c.Byte(nullable: false),
                        Projects_Id = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tags_Id, t.Projects_Id })
                .ForeignKey("dbo.Tags", t => t.Tags_Id, cascadeDelete: true)
                .ForeignKey("dbo.Projects", t => t.Projects_Id, cascadeDelete: true)
                .Index(t => t.Tags_Id)
                .Index(t => t.Projects_Id);
            
            DropColumn("dbo.Tags", "Project_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tags", "Project_Id", c => c.Byte());
            DropForeignKey("dbo.TagsProjects", "Projects_Id", "dbo.Projects");
            DropForeignKey("dbo.TagsProjects", "Tags_Id", "dbo.Tags");
            DropIndex("dbo.TagsProjects", new[] { "Projects_Id" });
            DropIndex("dbo.TagsProjects", new[] { "Tags_Id" });
            DropTable("dbo.TagsProjects");
            CreateIndex("dbo.Tags", "Project_Id");
            AddForeignKey("dbo.Tags", "Project_Id", "dbo.Projects", "Id");
        }
    }
}
